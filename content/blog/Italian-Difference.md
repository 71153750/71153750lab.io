---
title: "Educational Difference"
date: 2019-07-13T07:58:19+02:00
summary: Some notes on briefly educated in Italy
Draft: true
tags: ["education"]
---
And then he said, "and then wait until the water boils off completely!" And the class erupted with laughter. The punchline to a joke a professor told our class one morning about a Russian colleague explaining that he knew how to make pasta properly. Being a master's student at Politecnico di Milano has  been my best in-classroom educational experience at university. Not only because of the good humoured professors but also experiencing a well thought out system. 

Uniquely privileged, I completed my undergraduate degree in South Africa at the University of the Witwatersrand, Wits for short and the name everyone uses, and then later began and am still doing a master's program at Tsinghua University in Beijing. To compare the three is not really possible for they all offer something special in their context and were experienced differently by me. I feel compelled to share a bit about Politecnico. Not because I think it's the best, but because it does some aspects rather well. 

Drowning in the deep end is how I often felt during my undergraduate degree. Courses were these insurmountable mountains that needed to be summited before before transported to the base of new, often very different mountain. The Italian leg of my master's has felt more like drinking from a waterfall. I haven't any point really felt I was in deep danger of failing and the desperation mixed with moments of hopelessness that permeated through my first years at university. 

I know you want to point out the flaws in my argument and to my inherent bias. However, I'm not your typical South African transplant quick to say everything outside of South Africa is better because it's not. But somethings are I think it's worth recognising where gaps exist. Italians do universities better than we do. 

At Wits it was just too easy to fail and be held back a year. We were told this is just how it was and that they were preparing us for the workplace. A new student doesn't know better but after seeing friends fail auxiliary courses that I still haven't come to understand the significance of I've begun to wonder if any of it was necessary? As a Wits student I would've been shocked to know that in Italy I'd have five possibilities to pass an exam. And that although conditions differ between courses failing an exam in your first semester won't mean I'll have to panic about a supplementary exam at the end. In fact my first two opportunities will have some flexibility and by offering this flexibility and safety a university like Politecnico removes the peak stress and fear along with eliminating the massive degree extending delays built into the Wit's system. Exams are still scary but at least my life didn't hang in the balance like it so often did before. I never wrote a supplementary exam at Wits so I can't speak to the experience but to have only two chances, often several months apart. The second chance only permitt




