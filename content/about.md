---
title:
---
# Hi,

My name is Tiisetso Daniel Murray.

I've always been envious of professions where people can demonstrate their portfolios of work. I have a bachelor's degree in industrial engineering and there is not is always something to simply show. 

This is blog is my partial answer to that. I'm not a conventional engineer. In my spare time I've bred mealworms in self harvesting farm, grown herbs hydroponically using the thin film nutrient method, and built my own workshop from scratch.  Here though, I will share thoughts and ideas as I explore the world and my own identity. Writing as a pursuit within itself for whomever finds their way here. 

Thank you for reading.
